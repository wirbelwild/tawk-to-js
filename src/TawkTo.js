/*!
 * Bit&Black Tawk.to
 *
 * @copyright Copyright (c), Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

/**
 * @param propertyId The ID of one of your channels. For example if your direct chat link 
 *                   would be `https://tawk.to/chat/s0m3r34llyw31r3d1d/default`, then your ID would be
 *                   `s0m3r34llyw31r3d1d/default`.
 * @constructor
 */
function TawkTo(propertyId) {
    window.Tawk_API = window.Tawk_API || {};
    window.Tawk_LoadStart = new Date();

    let script = document.createElement("script"),
        scriptFirst = document.getElementsByTagName("script")[0]
    ;

    script.async = true;
    script.src = `https://embed.tawk.to/${propertyId}`;
    script.setAttribute("crossorigin", "*");
    scriptFirst.parentNode.insertBefore(script, scriptFirst);
}

export default TawkTo;