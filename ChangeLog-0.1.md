# Changes in Bit&Black Tawk.to 0.1

## 0.1.0 2021-10-09

### Changed

- Change handling of the chat ID so that IDs can be handeled that don't end with `/default`.