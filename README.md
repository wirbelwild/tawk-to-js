[![Codacy Badge](https://app.codacy.com/project/badge/Grade/c5ac95b9628647a9a2d3d9985b39136a)](https://www.codacy.com/bb/wirbelwild/tawk-to-js/dashboard)
[![npm version](https://badge.fury.io/js/bitandblack-tawk-to.svg)](https://badge.fury.io/js/bitandblack-tawk-to)

# Bit&Black Tawk.to 

Adds the Tawk.to Plugin to your Website.

## Installation 

This library is made for the use with [Node](https://www.npmjs.com/package/bitandblack-tawk-to). Add it to your project by running `$ npm install bitandblack-tawk-to` or `$ yarn add bitandblack-tawk-to`.

## Usage 

Import the library and initialize like that: 

````javascript
import TawkTo from "bitandblack-tawk-to";

const { TawkTo } = new TawkTo("s0m3r34llyw31r3d1d/default");
````

The parameter with that really weired string is your property id.

## Help 

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).